import random

from celery import shared_task

from django.utils import timezone

from bot.automation import BrowserAutomator
from reviews.models import Item, Account


@shared_task
def submit_review():
    items = Item.objects.filter(active=True)
    if not items:
        return

    now = timezone.now()
    # find item with minimum number of posted reviews
    min_reviews_posted_count = 999999999
    item = items[0]
    for i in items:
        reviews_count_today = i.reviews.filter(
            posted=True,
            posted_at__year=now.year,
            posted_at__month=now.month,
            posted_at__day=now.day
        ).count()
        reviews_count_posted = i.reviews.filter(posted=True).count()
        new_reviews = i.reviews.filter(posted=False)

        if reviews_count_today >= i.max_reviews_per_day:
            continue

        if reviews_count_posted > i.max_reviews:
            i.active = False
            i.save()
            continue

        if reviews_count_posted < min_reviews_posted_count:
            min_reviews_posted_count = i.reviews.count()
            item = i

        # do not iterate anymore, we already have item with zero reviews posted
        if reviews_count_posted == 0 and min_reviews_posted_count == 0 and len(new_reviews):
            item = i
            break

    account = random.choice(list(Account.objects.filter(used_for_signup=False, valid=True)))
    review = random.choice(list(item.reviews.filter(posted=False)))
    automator = BrowserAutomator(account=account, review=review)
    automator.run()
    if automator.success:
        account.signup_at = timezone.now()
        account.signup_ip = automator.account_signup_ip
        account.login_at = timezone.now()
        account.used_for_signup = True
        review.account = account
        review.posted = True
        review.posted_ip = automator.review_posted_ip
        review.posted_at = timezone.now()
        if review.item.reviews.count() >= review.item.max_reviews:
            review.item.active = False
            review.item.save()
    else:
        account.valid = False

    account.save()
    review.save()
