import uuid
import json
import zipfile
import random
import os
import time
import logging

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import NoSuchElementException

from django.conf import settings

from bot.utils import USER_AGENTS
from bot.exceptions import TargetComEmailNotUniqueException
from configurations.models import LuminatiCredentials


logger = logging.getLogger(__name__)
luminati = LuminatiCredentials.objects.last()


class BrowserAutomator:
    def __init__(self, account, review, retries_max=10):
        self.account = account
        self.review = review
        self.unique_id = uuid.uuid4().hex
        self.original_file_path = os.path.join(
            settings.BASE_DIR,
            'browser_extension/background.js'
        )
        self.new_file_path = os.path.join(
            settings.BASE_DIR,
            f'/tmp/background-{self.unique_id}.js'
        )
        self.extension_file_path = os.path.join(
            settings.BASE_DIR,
            f'/tmp/proxy-{self.unique_id}.zip'
        )
        self.manifest_file_path = os.path.join(
            settings.BASE_DIR,
            f'browser_extension/manifest.json'
        )
        self.create_new_proxy_extension()
        self.driver = self.init_driver()
        self.success = False
        self.retries_max = retries_max
        self.retries = 0
        self.review_posted_ip = None
        self.account_signup_ip = None

    def init_driver(self):
        chrome_options = Options()
        chrome_options.add_extension(self.extension_file_path)
        user_agent = random.choice(USER_AGENTS)
        chrome_options.add_argument(f'user-agent={user_agent}')
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--disable-setuid-sandbox")
        prefs = {"profile.managed_default_content_settings.images": 2}
        chrome_options.add_experimental_option("prefs", prefs)
        # driver_path = os.path.join(settings.BASE_DIR, 'drivers/chromedriver_mac')
        driver_path = '/usr/local/bin/chromedriver'
        driver = webdriver.Chrome(executable_path=driver_path, options=chrome_options)
        return driver

    def make_copy_of_js_file(self):
        with open(self.original_file_path) as original_file:
            with open(self.new_file_path, 'w') as new_file:
                suffix = f'-country-gb-session-{random.randint(99, 99999999)}'
                js_code = original_file.read()
                updated_js_code = js_code.replace(
                    '[PROXY_USERNAME]',
                    f'{luminati.username}-{suffix}'
                ).replace(
                    '[PROXY_PASSWORD]',
                    luminati.password
                ).replace(
                    '[PROXY_HOST]',
                    luminati.host
                ).replace(
                    '[PROXY_PORT]',
                    luminati.port
                )
                new_file.write(updated_js_code)

    def create_new_proxy_extension(self):
        self.make_copy_of_js_file()

        with zipfile.ZipFile(self.extension_file_path, 'w', zipfile.ZIP_DEFLATED) as f:
            f.write(self.new_file_path, 'background.js')
            f.write(self.manifest_file_path, 'manifest.json')

    def run(self):
        while self.retries <= self.retries_max:
            try:
                self.signup()
                self.go_to_item_page()
                self.submit_review()
                self.success = True
                break
            except TargetComEmailNotUniqueException as e:
                self.close_browser()
                self.remove_temporary_files()
                logger.error(f'{self.account} email already exists. Deactivating account..')
                self.account.valid = False
                self.account.save()
                return
            except Exception as e:
                self.retries += 1
                self.close_browser()
                self.remove_temporary_files()
                self.create_new_proxy_extension()
                self.driver = self.init_driver()
                logger.exception(f'Error submitting {self.review} with {self.account}. '
                                 f'Retry #{self.retries}. Error: {e}')

        self.close_browser()
        self.remove_temporary_files()
        time.sleep(3)
        self.success = True

    def signup(self):
        self.go_to_signup_page()
        self.submit_signup_form()
        ip_data = self.get_my_ip()
        self.account_signup_ip = ip_data['ip']

    def remove_temporary_files(self):
        if os.path.exists(self.new_file_path):
            os.remove(self.new_file_path)
        if os.path.exists(self.extension_file_path):
            os.remove(self.extension_file_path)

    def go_to_signup_page(self):
        self.driver.get('https://www.target.com/')
        self.driver.find_element_by_id('account').click()
        time.sleep(2)
        sign_up_btn = WebDriverWait(self.driver, 10).until(
            ec.element_to_be_clickable((By.ID, "accountNav-createAccount"))
        )
        sign_up_btn.click()

    def submit_signup_form(self):
        username_input = WebDriverWait(self.driver, 10).until(
            ec.element_to_be_clickable((By.ID, "username"))
        )

        username_input.send_keys(self.account.email)
        time.sleep(2)
        self.driver.find_element_by_id('firstname').send_keys(self.account.first_name)
        time.sleep(2)

        self.driver.find_element_by_id('lastname').send_keys(self.account.last_name)
        time.sleep(2)

        self.driver.find_element_by_id('password').send_keys(self.account.password)
        time.sleep(2)

        self.driver.find_element_by_css_selector('.nds-checkbox').click()
        time.sleep(3)
        self.driver.find_element_by_id('password').send_keys(Keys.ENTER)
        time.sleep(3)

        try:
            email_exists_message = self.driver.find_element_by_xpath(
                '//*[@id="root"]/div/div[1]/div/div[2]/div/div'
            )
            if email_exists_message.is_displayed():
                raise TargetComEmailNotUniqueException()
        except NoSuchElementException:  # means everything goes well
            ...

        skip_btn = WebDriverWait(self.driver, 10).until(
            ec.element_to_be_clickable((By.ID, "circle-skip"))
        )
        skip_btn.click()
        time.sleep(5)

    def go_to_item_page(self):
        self.driver.get(self.review.item.url)
        write_review_btn = WebDriverWait(self.driver, 10).until(
            ec.element_to_be_clickable((By.ID, "pdp-write-review"))
        )
        write_review_btn.click()
        time.sleep(2)

    def submit_review(self):
        self.driver.find_element_by_css_selector('div[class^=RatingStar] > '
                                                 'svg > a:nth-child(5)').click()
        time.sleep(2)

        self.driver.find_element_by_id('reviewDesc').send_keys(self.review.text)
        time.sleep(2)

        self.driver.find_element_by_id('guestName').send_keys(self.account.first_name)
        time.sleep(2)

        self.driver.find_element_by_id('submitReview').click()
        time.sleep(2)

        ip_data = self.get_my_ip()
        self.review_posted_ip = ip_data['ip']

    def get_my_ip(self):
        self.driver.get('https://api.myip.com/')
        time.sleep(1)
        body = WebDriverWait(self.driver, 10).until(
            ec.element_to_be_clickable((By.CSS_SELECTOR, "body"))
        )
        data = json.loads(body.text)

        return data  # {"ip":"11.1.11.11","country":"country","cc":"country code"}

    def close_browser(self):
        try:
            self.driver.close()
            self.driver.quit()
        except:
            self.remove_temporary_files()
            logger.exception(f'Error closing browser {self.unique_id}.')
