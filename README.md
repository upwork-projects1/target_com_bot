## Docker 
_(preferred way to run app)_

```docker-compose up --build```

## Manual
**CELERY**

`celery -A target_com_bot worker -n worker1 -l INFO -P eventlet -c 50 --prefetch-multiplier=50`

`celery -A target_com_bot worker -n worker2 -l INFO -P eventlet -c 50 --prefetch-multiplier=50`

`celery -A target_com_bot beat -l INFO`

**FLOWER**

`flower -A target_com_bot --port=5555`

**REDIS**

`redis-server`

**DJANGO DEV SERVER**

`python manage.py runserver 0.0.0.0:8000`
