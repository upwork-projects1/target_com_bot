import csv

from django.views.generic import FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.urls import reverse_lazy

from .forms import ReviewsFileForm
from .models import Review, Item


class UploadReviewsFileFormView(LoginRequiredMixin, FormView):
    form_class = ReviewsFileForm
    template_name = 'reviews/file_upload.html'
    success_url = reverse_lazy('reviews:upload')

    def form_valid(self, form):
        reviews_file = form.save(commit=False)
        reviews_file.user = self.request.user
        reviews_file.save()

        rows = []
        rows_count = 0
        with open(reviews_file.document.path) as f:
            reader = csv.reader(f, delimiter=';')
            for r in reader:
                rows_count += 1
                if rows_count == 1:
                    continue
                rows.append(r)

        items_to_create = list()
        existing_items = list()
        for row in rows:
            item_url = row[0]
            item_from_db = Item.objects.filter(url=item_url).first()
            if item_from_db:
                existing_items.append(item_from_db)
                continue

            if item_url in {i.url for i in items_to_create}:
                continue

            max_reviews_per_day = next((i[1].strip() for i in rows if i[1].strip()))
            max_reviews = next((i[2].strip() for i in rows if i[2].strip()))
            items_to_create.append(
                Item(
                    url=item_url,
                    max_reviews_per_day=max_reviews_per_day,
                    max_reviews=max_reviews
                )
            )
        new_items = Item.objects.bulk_create(items_to_create)
        new_items = Item.objects.filter(id__in={i.id for i in new_items})
        items = set(list(new_items) + list(existing_items))

        reviews = list()
        for item in items:
            reviews_texts = [r[3] for r in rows if r[0] == item.url]
            for review_text in reviews_texts:
                reviews.append(
                    Review(
                        item=item,
                        reviews_file=reviews_file,
                        text=review_text
                    )
                )
        Review.objects.bulk_create(reviews)

        messages.success(self.request, f'File Uploaded. Total lines: {rows_count}')
        return super().form_valid(form)
