from django.contrib import admin

from .models import Item, Review, ReviewsFile


@admin.register(ReviewsFile)
class ReviewsFileAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'user', 'uploaded_at')
    list_filter = ('uploaded_at', )
    date_hierarchy = 'uploaded_at'


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'url', 'max_reviews_per_day',
                    'max_reviews', 'created_at')
    list_filter = ('created_at', )
    date_hierarchy = 'created_at'


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'item', 'account', 'posted',
                    'posted_at', 'created_at',)
    list_filter = ('posted_at', 'created_at', 'posted')
    date_hierarchy = 'created_at'
