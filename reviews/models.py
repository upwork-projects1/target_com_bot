import os

from django.db import models
from django.conf import settings
from django.dispatch import receiver
from django.core.validators import FileExtensionValidator

from accounts.models import Account


class ReviewsFile(models.Model):
    document = models.FileField(upload_to='uploaded/',
                                validators=[FileExtensionValidator(allowed_extensions=['csv'])])
    uploaded_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return f'ReviewsFile <{self.id}>'


class Item(models.Model):
    url = models.URLField()
    max_reviews_per_day = models.IntegerField()
    max_reviews = models.IntegerField()
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'Item <{self.id}>'


class Review(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE, related_name='reviews')
    account = models.ForeignKey(Account, on_delete=models.SET_NULL, blank=True,
                                null=True, related_name='reviews')
    reviews_file = models.ForeignKey(ReviewsFile, on_delete=models.CASCADE,
                                     related_name='reviews')
    text = models.CharField(max_length=3000)
    posted = models.BooleanField(default=False)
    posted_ip = models.GenericIPAddressField(blank=True, null=True)
    posted_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'Review <{self.id}>'


# *************  SIGNALS  ************* #

@receiver(models.signals.post_delete, sender=ReviewsFile)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `ReviewsFile` object is deleted.
    """
    if instance.document:
        if os.path.isfile(instance.document.path):
            os.remove(instance.document.path)


@receiver(models.signals.pre_save, sender=ReviewsFile)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """
    Deletes old file from filesystem
    when corresponding `ReviewsFile` object is updated
    with new file.
    """
    if not instance.pk:
        return False

    try:
        old_file = ReviewsFile.objects.get(pk=instance.pk).document
    except ReviewsFile.DoesNotExist:
        return False

    new_file = instance.document
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)
