from django.urls import path

from . import views

app_name = 'reviews'

urlpatterns = [
    path('upload/', views.UploadReviewsFileFormView.as_view(),
         name='upload'),
]
