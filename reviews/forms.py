from django import forms

from .models import ReviewsFile


class ReviewsFileForm(forms.ModelForm):
    document = forms.FileField(widget=forms.FileInput(attrs={'accept': ".csv"}))

    class Meta:
        model = ReviewsFile
        fields = ('document',)
