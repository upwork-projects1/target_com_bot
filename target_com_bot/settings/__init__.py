from __future__ import absolute_import, unicode_literals

from target_com_bot.celery import app as celery_app

__all__ = ('celery_app', )
