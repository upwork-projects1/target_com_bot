from __future__ import absolute_import, unicode_literals

import os

from decouple import config

from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', config('DJANGO_SETTINGS_MODULE'))

app = Celery('target_bot_settings')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()

app.conf.beat_schedule = {
    'add_random_people_data': {
        'task': 'accounts.tasks.add_random_people_data',
        'schedule': 600,
    },
    'submit_review': {
        'task': 'bot.tasks.submit_review',
        'schedule': 300,
    },
}
