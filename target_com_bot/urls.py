from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth import views as auth_views
from django.views.generic import RedirectView
from django.urls import reverse_lazy

admin.site.site_title = 'Target.com Bot Admin'
admin.site.site_header = 'Target.com Bot Admin'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('reviews/', include('reviews.urls')),
    path('', RedirectView.as_view(url=reverse_lazy('reviews:upload'), permanent=False),
         name='index')
]

if settings.DEBUG:
    urlpatterns += [
        *static(settings.STATIC_URL, document_root=settings.STATIC_ROOT),
        *static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    ]
