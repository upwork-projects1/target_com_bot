FROM python:3.7-slim

RUN mkdir /app
WORKDIR /app

RUN apt-get -y update && apt-get -y install gnupg2 wget curl libpq-dev gcc

# install google chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
RUN apt-get -y update
RUN apt-get install -y google-chrome-stable xvfb

# install chromedriver
RUN apt-get install -yqq unzip
RUN wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip
RUN unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/ && rm -f /tmp/chromedriver.zip
RUN perl -pi -e 's/cdc_/gfb_/g' /usr/local/bin/chromedriver

ENV DISPLAY=:0

COPY requirements.txt /app/

RUN pip install --upgrade pip && pip install -r requirements.txt && rm -rf ~/.cache/pip/*

COPY . /app/

CMD ["sh", "deployment/start-prod-server.sh"]
