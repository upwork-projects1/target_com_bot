import random

import requests

from celery import shared_task
from unidecode import unidecode

from .models import Account


@shared_task
def add_random_people_data():
    email_domains = (
        'gmail.com',
        'yahoo.com',
        'aol.com',
        'outlook.com',
        'icloud.com',
        'mail.com',
        'zoho.com',
        'hotmail.com',
        'hotmail.co.uk',
        'msn.com',
        'live.com',
        'ymail.com',
        'verizon.net',
        'rocketmail.com',
        'att.net',
        'yahoo.ca',
        'sky.com'
    )
    # get count of accounts , that can be used to sign up
    new_accounts_count = Account.objects.filter(
        used_for_signup=False,
        valid=True
    ).count()
    if new_accounts_count > 100000:
        return

    # r = requests.get('https://randomuser.me/api/?results=1000&nat=au,ca,gb,us&'
    r = requests.get('http://randomuser:3000/api/?results=1000&nat=au,ca,gb,us&'
                     'password=upper,lower,number,9-20&'
                     'inc=name,nat,email,login,phone')
    results = r.json()['results']
    objects_to_create = list()
    existing_emails = set(Account.objects.values_list('email', flat=True))
    for item in results:
        item['email'] = item['email'].replace('example.com',
                                              random.choice(email_domains))
        if item['email'] not in existing_emails:
            objects_to_create.append(
                Account(
                    first_name=unidecode(item['name']['first']),
                    last_name=unidecode(item['name']['last']),
                    email=item['email'],
                    password=item['login']['password'],
                    phone=item['phone'],
                    nationality=item['nat']
                )
            )
            existing_emails.add(item['email'])

    Account.objects.bulk_create(objects_to_create)
