from django.contrib import admin

from .models import Account


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at', 'updated_at')
    list_display = ('__str__', 'email', 'first_name', 'last_name', 'valid')
    search_fields = ('email', 'first_name', 'last_name')
    list_filter = ('nationality', 'used_for_signup', 'signup_at',
                   'created_at', 'valid')
    date_hierarchy = 'created_at'
