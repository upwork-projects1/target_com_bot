from django.db import models


class Account(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField()
    password = models.CharField(max_length=25)
    phone = models.CharField(max_length=30)
    nationality = models.CharField(max_length=3)
    valid = models.BooleanField(default=True)
    signup_ip = models.GenericIPAddressField(blank=True, null=True)
    used_for_signup = models.BooleanField(default=False)
    signup_at = models.DateTimeField(blank=True, null=True)
    login_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'Account <{self.id}>'
