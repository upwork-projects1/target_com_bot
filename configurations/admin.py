from django.contrib import admin

from .models import LuminatiCredentials


@admin.register(LuminatiCredentials)
class LuminatiCredentialsAdmin(admin.ModelAdmin):
    list_display = ('username', 'host', 'port')
