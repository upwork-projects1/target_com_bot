from django.db import models
from django.core.exceptions import ValidationError


class LuminatiCredentials(models.Model):
    username = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    host = models.CharField(max_length=200, default='zproxy.lum-superproxy.io')
    port = models.IntegerField(default=22225)

    class Meta:
        verbose_name_plural = 'Luminati Credentials'

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        if not self.pk and LuminatiCredentials.objects.exists():
            raise ValidationError('Only one LuminatiCredentials instance can exist.')

        return super().save(force_insert, force_update, using, update_fields)
