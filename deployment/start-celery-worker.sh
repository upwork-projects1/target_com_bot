#!/bin/bash

export DISPLAY=:0
Xvfb :0 -screen 0 1024x768x24 &
celery -A target_com_bot worker -n $WORKER_NAME -l info -c 3 --prefetch-multiplier=2
