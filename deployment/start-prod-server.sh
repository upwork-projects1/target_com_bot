#!/bin/bash

python manage.py migrate --noinput
python manage.py collectstatic --noinput
gunicorn target_com_bot.wsgi:application -c /app/deployment/gunicorn.conf.py
