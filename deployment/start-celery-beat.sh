#!/bin/bash

rm -f /opt/celerybeat.pid
rm -f /opt/celerybeat-schedule
celery -A target_com_bot beat -l info --pidfile=/opt/celerybeat.pid --schedule=/opt/celerybeat-schedule
